#ifndef COPYPARTICLE2PVRELATIONSFROMLINEPERSISTENCELOCATIONS_H
#define COPYPARTICLE2PVRELATIONSFROMLINEPERSISTENCELOCATIONS_H 1

#include <set>
#include <string>
#include <vector>

#include <Event/HltDecReports.h>
#include <Kernel/ILinePersistenceSvc.h>

#include "CopyParticle2PVRelations.h"

/** @class CopyParticle2PVRelationsFromLinePersistenceLocations CopyParticle2PVRelationsFromLinePersistenceLocations.h
 *
 * @brief Clone TES locations, of LHCb::Particle to LHCb::RecVertex relations
 * tables, defined by the ILinePersistenceSvc.
 *
 * Most of the actual cloning logic of this algorithm is implemented in the
 * parent class, CopyParticle2PVRelations. This algorithm effectively
 * 'interfaces' the base class with the ILinePersistenceSvc.
 *
 * Example usage:
 *
 * @code
 * from Configurables import HltLinePersistenceSvc
 * p2pv_cloner = CopyParticle2PVRelationsFromLinePersistenceLocations(
 *     OutputPrefix='/Event/SomeStreamName',
 *     # Clone the locations requested by one Turbo line
 *     LinesToCopy=['Hlt2CharmHadD02KmPipTurbo'],
 *     # Get the list of locations to copy from the TCK
 *     ILinePersistenceSvc='TCKLinePersistenceSvc',
 *     # Clone the related RecVertex objects using a cloner that clears
 *     # the container of tracks on each cloned vertex
 *     ClonerType='VertexBaseFromRecVertexClonerNoTracks'
 * )
 * @endcode
 *
 * @see CopyParticle2PVRelations as the parent class
 * @see CopyLinePersistenceLocations for cloning KeyedContainer objects
 */
class CopyParticle2PVRelationsFromLinePersistenceLocations :
  public CopyParticle2PVRelations {

  public:
    CopyParticle2PVRelationsFromLinePersistenceLocations(const std::string& name, ISvcLocator* svcLocator);

    StatusCode initialize() override;

    StatusCode execute() override;

  private:
    /// Implementation of ILinePersistenceSvc used to get the list of locations to clone.
    Gaudi::Property<std::string> m_linePersistenceSvcName{this, "ILinePersistenceSvc", "TCKLinePersistenceSvc"};

    /// List of HLT2 lines whose outputs are to be copied.
    Gaudi::Property<std::vector<std::string>> m_linesToCopy{this, "LinesToCopy", {}};

    std::set<std::string> m_linesToCopySet;

    /// TES location of the HltDecReports object to give to ILinePersistenceSvc.
    Gaudi::Property<std::string> m_hltDecReportsLocation{this, "Hlt2DecReportsLocation", LHCb::HltDecReportsLocation::Hlt2Default};

    SmartIF<ILinePersistenceSvc> m_linePersistenceSvc;
};
#endif // COPYPARTICLE2PVRELATIONSFROMLINEPERSISTENCELOCATIONS_H
