// $Id: CopyParticle2RelatedInfo.cpp,v 1.1 2010-02-24 15:36:23 jpalac Exp $
// Include files 
#include "CopyParticle2RelatedInfo.h"

DECLARE_COMPONENT_WITH_ID( CopyParticle2RelatedInfo, "CopyParticle2RelatedInfo" )

const std::string Defaults<Particle2RelatedInfo::Table>::relationsName = "/Particle2CV1Relations";
const std::string Location<Particle2RelatedInfo::Table>::Default = "NO DEFAULT LOCATION";
