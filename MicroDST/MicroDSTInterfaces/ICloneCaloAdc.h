// $Id: ICloneCaloAdc.h,v 1.1 2017-06-15 11:25:05 rvazquez Exp $
#ifndef MICRODST_ICLONECALOADC_H 
#define MICRODST_ICLONECALOADC_H 1

// from MicroDST
#include "MicroDST/ICloner.h"

// Forward declarations
namespace LHCb 
{
  class CaloAdc;
}

/** @class ICloneCaloAdc MicroDST/ICloneCaloAdc.h
 *  
 *
 *  @author Ricardo Vazquez Gomez
 *  @date   2017-06-15
 */
class GAUDI_API ICloneCaloAdc : virtual public MicroDST::ICloner<LHCb::CaloAdc>
{

public: 

  /// Interface ID
  DeclareInterfaceID(ICloneCaloAdc, 1, 0 );

  /// Destructor
  virtual ~ICloneCaloAdc() { }

};

#endif // MICRODST_ICLONECALOADC_H
