################################################################################
# Package: DaVinciTools
################################################################################
gaudi_subdir(DaVinciTools)

gaudi_depends_on_subdirs(Phys/DaVinciKernel
                         Phys/LoKiCore)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(DaVinciTools
                 src/*.cpp
                 LINK_LIBRARIES DaVinciKernelLib LoKiCoreLib)

gaudi_install_python_modules()
