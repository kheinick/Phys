from Configurables import (TaggerCharmTool,
                           )

from FlavourTagging.ClassicTunings import setTuningProperties

# classic tunings
Stripping21 = TaggerCharmTool(name="OSCharmTagger_Stripping21")
setTuningProperties(Stripping21, 'Stripping21')

Stripping23 = TaggerCharmTool(name="OSCharmTagger_Stripping23")
setTuningProperties(Stripping23, 'Stripping23')
