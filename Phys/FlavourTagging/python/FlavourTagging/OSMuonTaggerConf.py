from Configurables import (TaggerMuonTool,
                           OSMuonTagger,
                           OSMuonClassifierFactory,
                           )

from FlavourTagging.ClassicTunings import setTuningProperties

# Tuning definitions
default = TaggerMuonTool(name="OSMuonTagger")

# Classic Tunings
Stripping23 = TaggerMuonTool(name="OSMuonTagger_Stripping23")
setTuningProperties(Stripping23, 'Stripping23')

Stripping21 = TaggerMuonTool(name='OSMuonTagger_Stripping21')
setTuningProperties(Stripping21, 'Stripping21')

Development = OSMuonTagger(name='OSMuonTagger_Development')
Development.addTool(OSMuonClassifierFactory, name='MuonMVA')
Development.MuonMVA.ClassifierType = 'TMVA'
Development.MuonMVA.ClassifierName = 'OSMuon_Data_Run2_All_Bu2JpsiK_XGB_BDT_v1r0'

Development.Aliases = {
    "partP": "P/GeV",
    "partPt": "PT/GeV",
    "ptB": "Signal_PT/GeV",
    "IPs": "IPSig",
    "partlcs": "TRCHI2DOF",
    "PIDNNm": "PROBNNmu",
    "ghostProb": "TRGHP",
    "IPPU": "IPPUSig",
    "mult": "countTracks",
    "mva1": "MVA[{}]("
        "mult, log(partP), log(partPt), log(ptB), log(IPs), log(partlcs),"
        "PIDNNm, log(ghostProb), log(IPPU)"
        ")".format(Development.MuonMVA.getFullName()),
    }

Development.SelectionPipeline = [
        [
            "ABSID==13",
            "P/GeV>0",
            "PT/GeV>1.1",
            "IsSignalDaughter==0",
        ],
        [
            "minPhiDistance>0.005",  # same cut as preselection,
            "MuonPIDIsMuon==1",
            "TRCHI2DOF<3",
            "IPSig>0",
            "IPPU>3",
            "TRGHP<0.4",
            "PROBNNmu>0.35",
            "PROBNNpi<0.8",
            "PROBNNe<0.8",
            "PROBNNk<0.8",
            "PROBNNp<0.8",
            ],
        [
            "mult",
            "partP",
            "partPt",
            "ptB",
            "IPs",
            "partlcs",
            "PIDNNm",
            "ghostProb",
            "IPPU",
            "mva1",
            ],
        ]


Summer2017Opt = OSMuonTagger(name='OSMuonTagger_Summer2017Opt')
Summer2017Opt.addTool(OSMuonClassifierFactory, name='MuonMVA')
Summer2017Opt.MuonMVA.ClassifierType = 'TMVA'
Summer2017Opt.MuonMVA.ClassifierName = 'OSMuon_Data_Run2_All_Bu2JpsiK_XGB_BDT_v2r0'

Summer2017Opt.Aliases = {
    'mva1': 'MVA[{}]('
            'P, PT, IPPUSig, TRGHP, PROBNNmu_MC15TuneV1, AbsIP, countTracks,'
            'Signal_PT, IPSig'
            ')'.format(Summer2017Opt.MuonMVA.getFullName()),
}

# cuts optimised using skopt's gaussian processes
Summer2017Opt.SelectionPipeline = [
        [
            "ABSID==13",
            "P>2540",
            "PT>951",
            "IsSignalDaughter==0",
            "minPhiDistance>0.0029",
            "MuonPIDIsMuon==1",
            "TRCHI2DOF<3",
            "IPSig>0.437",
            "PIDmu>-100",
            "IPPUSig>3.91",
            "TRGHP<0.369",
            "PROBNNmu>0.798",
            "PROBNNpi<0.956",
            "PROBNNe<0.521",
            "PROBNNk<0.952",
            "PROBNNp<0.954",
            ],
        [
            'P',
            'PT',
            'IPPUSig',
            'TRGHP',
            'PROBNNmu_MC15TuneV1',
            'AbsIP',
            'countTracks',
            'Signal_PT',
            'IPSig',
            'mva1',
            ],
]

Summer2017Opt_v2_Run2 = OSMuonTagger(name='OSMuonTagger_Summer2017Opt_v2_Run2')
Summer2017Opt_v2_Run2.addTool(OSMuonClassifierFactory, name='MuonMVA')
Summer2017Opt_v2_Run2.MuonMVA.ClassifierType = 'TMVA'
Summer2017Opt_v2_Run2.MuonMVA.ClassifierName = 'OSMuon_Data_Run2_All_Bu2JpsiK_XGB_BDT_v1r0'

Summer2017Opt_v2_Run2.Aliases = {
    "BPVIPCHI2": "BPVIPCHI2()",
    'mva1': 'MVA[{}]('
    'P, PT, IPPUSig, TRGHP, PROBNNmu_MC15TuneV1, AbsIP, countTracks,'
    'Signal_PT, IPSig'
    ')'.format(Summer2017Opt_v2_Run2.MuonMVA.getFullName()),
    }

# cuts optimised using skopt's gaussian processes
Summer2017Opt_v2_Run2.SelectionPipeline = [
    [
    "ABSID==13",
    "P>2540",
    "PT>951",
    "IsSignalDaughter==0",
    "minPhiDistance>0.0029",
    "MuonPIDIsMuon==1",
    "TRCHI2DOF<3",
    "BPVIPCHI2()>0",
    "IPSig>0.437",
    "PIDmu>-100",
    "IPPUSig>3.91",
    "TRGHP<0.369",
    "PROBNNmu_MC15TuneV1>0.798",
    "PROBNNpi_MC15TuneV1<0.956",
    "PROBNNe_MC15TuneV1<0.521",
    "PROBNNk_MC15TuneV1<0.952",
    "PROBNNp_MC15TuneV1<0.954",
    ],
    [
    'P',
    'PT',
    'IPPUSig',
    'TRGHP',
    'PROBNNmu_MC15TuneV1',
    'AbsIP',
    'countTracks',
    'Signal_PT',
    'IPSig',
    'mva1',
    ],
    ]

Summer2017Opt_v1_Run2_Bu2D0pi = OSMuonTagger(name='OSMuonTagger_Summer2017Opt_v1_Run2_Bu2D0pi')
Summer2017Opt_v1_Run2_Bu2D0pi.addTool(OSMuonClassifierFactory, name='MuonMVA')
Summer2017Opt_v1_Run2_Bu2D0pi.MuonMVA.ClassifierType = 'TMVA'
Summer2017Opt_v1_Run2_Bu2D0pi.MuonMVA.ClassifierName = 'OSMuon_Data_Run2_All_Bu2D0pi_XGBoost_BDT_v1r0'

Summer2017Opt_v1_Run2_Bu2D0pi.Aliases = {
    "BPVIPCHI2": "BPVIPCHI2()",
    'mva1': 'MVA[{}]('
        'P, PT, IPPUSig, TRGHP, PROBNNmu_MC15TuneV1, AbsIP, countTracks,'
        'Signal_PT, IPSig'
        ')'.format(Summer2017Opt_v1_Run2_Bu2D0pi.MuonMVA.getFullName()),
        }

Summer2017Opt_v1_Run2_Bu2D0pi.SelectionPipeline = [
        [
        "IsSignalDaughter==0",
        "MuonPIDIsMuon==1",
        "TRCHI2DOF<3",
        "P>5397.607897632618",
        "PT>1224.5606842444723",
        "TRGHP<0.5853621904749581",
        "minPhiDistance>=0.001581592160920209",
        "IPSig>0.2765874553883941",
        "PROBNNmu_MC15TuneV1>0.7985306048702641",
        "PROBNNpi_MC15TuneV1<0.9103690603292043",
        "PROBNNe_MC15TuneV1<0.8816411823897747",
        "PROBNNk_MC15TuneV1<0.5582434679955008",
        "PROBNNp_MC15TuneV1<0.5044204208392812",
        "IPPUSig>2.818552714515416",
        ],
        [
    'P',
    'PT',
    'IPPUSig',
    'TRGHP',
    'PROBNNmu_MC15TuneV1',
    'AbsIP',
    'countTracks',
    'Signal_PT',
    'IPSig',
    'mva1',
    ],
        ]


# Tagger is using the correctly trained versions of the mistag evaluation BDT
Summer2017Opt_v3_Run2 = OSMuonTagger(name='OSMuonTagger_Summer2017Opt_v3_Run2')
Summer2017Opt_v3_Run2.addTool(OSMuonClassifierFactory, name='MuonMVA')
Summer2017Opt_v3_Run2.MuonMVA.ClassifierType = 'TMVA'
Summer2017Opt_v3_Run2.MuonMVA.ClassifierName = 'OSMuon_Data_Run2_All_Bu2JpsiK_XGB_BDT_v2r0'

Summer2017Opt_v3_Run2.Aliases = {
    "BPVIPCHI2": "BPVIPCHI2()",
    'mva1': 'MVA[{}]('
    'P, PT, IPPUSig, TRGHP, PROBNNmu_MC15TuneV1, AbsIP, countTracks,'
    'Signal_PT, IPSig'
    ')'.format(Summer2017Opt_v3_Run2.MuonMVA.getFullName()),
    }

# using the same cut parameters
Summer2017Opt_v3_Run2.SelectionPipeline = Summer2017Opt_v2_Run2.SelectionPipeline
