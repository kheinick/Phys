from Configurables import (OSVtxChTagger,
                           TaggerVertexChargeTool,
                           NNetTool_MLP,
                           TaggingUtils,
                           SVertexOneSeedTool,
                           )

from FlavourTagging.ClassicTunings import setTuningProperties

# classic tunings
Stripping21 = TaggerVertexChargeTool(name='OSVtxChTagger_Stripping21')
Stripping21.addTool(NNetTool_MLP)
Stripping21.addTool(SVertexOneSeedTool)
setTuningProperties(Stripping21, 'Stripping21')

Stripping23 = TaggerVertexChargeTool(name='OSVtxChTagger_Stripping23')
Stripping23.addTool(NNetTool_MLP)
Stripping23.addTool(SVertexOneSeedTool)
setTuningProperties(Stripping23, 'Stripping23')


# classic tunings
Development = OSVtxChTagger(name='OSVtxChTagger_Development')
