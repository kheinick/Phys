from Configurables import (TaggerJetSameTool,
                           )

from FlavourTagging.ClassicTunings import setTuningProperties

# classic tunings
default = TaggerJetSameTool(name='SSJetTagger')
