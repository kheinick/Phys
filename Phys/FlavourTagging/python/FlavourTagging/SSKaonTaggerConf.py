from Configurables import (TaggerKaonSameTool,
                           TaggerNEWKaonSameTool,
                           TaggerSSKaon_dev_Tool,
                           )

from FlavourTagging.ClassicTunings import setTuningProperties

# classic tunings
Stripping21 = TaggerKaonSameTool(name='SSKaonTagger')
setTuningProperties(Stripping21, 'Stripping21')

Stripping23NNet = TaggerNEWKaonSameTool(name='SSKaonTagger_Stripping23NNet')
setTuningProperties(Stripping23NNet, 'Stripping23')


Stripping23NNetDev = TaggerSSKaon_dev_Tool(name='SSKaonTaggerDev_Stripping23NNet')
setTuningProperties(Stripping23NNetDev, 'Stripping23')
