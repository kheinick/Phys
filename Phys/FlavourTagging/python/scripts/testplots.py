from __future__ import division, print_function

import argparse
import sys

import matplotlib
matplotlib.use('Agg')  # dont rely on xwindow

import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import ks_2samp

# prevent argparsing from ROOT
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True

import root_numpy as rnp


REPORT = """\
                         test     ref
    tagged events:   {ntagged:>8}{ntagged_ref:>8}
    untagged events: {nuntagged:>8}{nuntagged_ref:>8}
    efficiency / %:  {efficiency:>8.3f}{efficiency_ref:>8.3f}
    std:             {std:>8.3f}{std_ref:>8.3f}
    KS statistics:   {ks_stat}
    p-value:         {p_val}
"""


def get_args():
    parser = argparse.ArgumentParser(
        description='Compare Taggers in two different root files')
    parser.add_argument('-t', '--test-file', type=str, required=True,
        help="""Filename of the root file to test""")
    parser.add_argument('-r', '--reference-file', type=str, required=True,
        help="""Read distributions from a reference file. The branch names
        should match the test file.""")
    parser.add_argument('-o', '--output-directory', type=str, default='output',
        help="""Output directory for plots. Wont be created. Existing plots
        will be overwritten without prompt.""")
    parser.add_argument('--p-val-warning', type=float, default=0.05, help="""
        The program will exit with an error code if the distributions diverge
        from the reference with p-value greater than this threshold.""")
    return parser.parse_args()


def main():
    # switch this to false if something is broken
    clear_status = True
    args = get_args()

    tageta_branches = filter(
        lambda x: 'TAGETA' in x, rnp.list_branches(args.test_file))
    tagetas = rnp.root2array(args.test_file, branches=tageta_branches)

    reference_branches = filter(
        lambda x: 'TAGETA' in x, rnp.list_branches(args.reference_file))
    reference_etas = rnp.root2array(args.reference_file, branches=reference_branches)

    # warn if branches do not match
    if set(tageta_branches) != set(reference_branches):
        missing_in_reference = set(tageta_branches) - set(reference_branches)
        missing_in_test = set(reference_branches) - set(tageta_branches)
        print(
            'WARNING: Branches do not match reference. Reference is missing '
            '{}. Test file is missing {}.'.format(
                missing_in_reference, missing_in_test))
        clear_status = False

    for branch in tageta_branches:
        # plot the reference
        reference_values = reference_etas[branch]
        reference_selection = reference_values != 0.5
        plt.hist(reference_values[reference_selection], bins=30, range=(0, 0.5),
                 normed=True, label='reference ({:d} entries)'.format(reference_selection.sum()))

        # plot the test distribution
        test_values = tagetas[branch]
        test_selection = test_values != 0.5
        hist, bins = np.histogram(test_values[test_selection], bins=30, range=(0, 0.5), normed=True)
        x = bins[:-1] + (bins[1] - bins[0]) / 2
        plt.errorbar(x, hist, fmt='o', label='test output ({:d} entries)'.format(test_selection.sum()))

        plt.title(branch)
        plt.xlabel('eta')
        plt.legend(loc='best')
        plt.savefig('{}/{}.pdf'.format(args.output_directory, branch))
        plt.close()
        print('Created plot for {}'.format(branch))
        ntagged_test = test_selection.sum()
        nuntagged_test = (~test_selection).sum()
        efficiency_test = ntagged_test / (ntagged_test + nuntagged_test)
        ntagged_ref = reference_selection.sum()
        nuntagged_ref = (~reference_selection).sum()
        efficiency_ref = ntagged_ref / (ntagged_ref + nuntagged_ref)
        ks_stat, p_val = ks_2samp(test_values, reference_values)

        if p_val < args.p_val_warning:
            print('WARNING: p-value for tagger "{}" too low. '
                  'Distributions diverge.'.format(branch))
            clear_status = False

        print(REPORT.format(
            ntagged=ntagged_test,
            nuntagged=nuntagged_test,
            efficiency=100 * efficiency_test,
            std=test_values[test_selection].std(),
            ntagged_ref=ntagged_ref,
            nuntagged_ref=nuntagged_ref,
            efficiency_ref=100 * efficiency_ref,
            std_ref=reference_values[reference_selection].std(),
            ks_stat=ks_stat,
            p_val=p_val,
            ))

    if not clear_status:
        sys.exit(1)


if __name__ == '__main__':
    main()
