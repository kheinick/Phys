#pragma once

#include "Kernel/DaVinciAlgorithm.h"

/** @class BTagging BTagging.h
 *
 *  Algorithm to tag the B flavour
 *
 *  @author Marco Musy
 *  @date   02/10/2006
 */

class BTagging : public DaVinciAlgorithm
{

public:

  /// Standard constructor
  using DaVinciAlgorithm::DaVinciAlgorithm;

  StatusCode execute() override;    ///< Algorithm execution

private:

  /// Run the tagging on the given location
  void performTagging(const std::string & location);

private:
  Gaudi::Property<std::string> m_tagLocation{this, "TagOutputLocation", 
  "FlavourTags",
  "Output location of the FlavourTag objects"};
};

