#ifndef PHYS_PHYS_FLAVOURTAGGING_ITAGGINGCLASSIFIER_H
#define PHYS_PHYS_FLAVOURTAGGING_ITAGGINGCLASSIFIER_H 1

// from STL
#include <vector>

class ITaggingClassifier{
public:
  /**
   * @brief      Main classification method
   *
   * Takes a vector of values of features and returns the corresponding MVA
   * output.
   *
   * @param[in]  featureValues  A vector of feature values
   *
   * @return     MVA classifier value
   */
  virtual double getClassifierValue(const std::vector<double>& featureValues) = 0;

};

#endif // PHYS_PHYS_FLAVOURTAGGING_ITAGGINGCLASSIFIER_H
