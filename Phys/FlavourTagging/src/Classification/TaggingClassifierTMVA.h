#ifndef PHYS_PHYS_FLAVOURTAGGING_TAGGINGCLASSIFIERTMVA_H
#define PHYS_PHYS_FLAVOURTAGGING_TAGGINGCLASSIFIERTMVA_H 1

#include <cstddef>

#include "ITaggingClassifier.h"

class TaggingClassifierTMVA : public ITaggingClassifier {

public:

  double getClassifierValue(const std::vector<double>& featureValues) override {
    return GetMvaValue(featureValues);
  }


  /**
   * @brief      Returns the classifier value
   *
   * @param[in]  featureValues  The feature values
   *
   * @return     The classifier value
   */
  virtual double GetMvaValue(const std::vector<double>& featureValues) const = 0;

  // returns classifier status
  bool IsStatusClean() const {
    return fStatusIsClean;
  }

protected:
  bool fStatusIsClean = true;

};

#endif // PHYS_PHYS_FLAVOURTAGGING_TAGGINGCLASSIFIERTMVA_H
