#ifndef PHYS_PHYS_FLAVOURTAGGING_TAGGINGCLASSIFIERXGB_H
#define PHYS_PHYS_FLAVOURTAGGING_TAGGINGCLASSIFIERXGB_H 1

// from STL
#include <string>

#include "ITaggingClassifier.h"

//#include "xgboost/c_api.h"

class TaggingClassifierXGB : public ITaggingClassifier {
  public:
    TaggingClassifierXGB();
    double getClassifierValue(const std::vector<double>& featureValues) override;
    void setPath(const std::string& path);

  private:
    //std::vector<float> m_predictionsCache = {0, 0};
    //DMatrixHandle m_cache_matrix,
    //              m_feature_matrix;
    //BoosterHandle m_booster;
};

#endif // PHYS_PHYS_FLAVOURTAGGING_TAGGINGCLASSIFIERXGB_H
