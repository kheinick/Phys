#pragma once

#include <vector>
#include <memory>
#include <mutex>

#include "Event/RecVertex.h"

struct FeatureGeneratorCache {
  public:

    static FeatureGeneratorCache& getInstance(){
      // see http://stackoverflow.com/a/11711991/2834918
      static FeatureGeneratorCache instance;
      return instance;
    }

    std::vector<const LHCb::RecVertex*> currentPileUpVertices;
    const LHCb::RecVertex::Range* currentOtherVertices;

    FeatureGeneratorCache(const FeatureGeneratorCache&) = delete;
    FeatureGeneratorCache(FeatureGeneratorCache&&) = delete;
    FeatureGeneratorCache& operator=(const FeatureGeneratorCache&) = delete;
    FeatureGeneratorCache& operator=(FeatureGeneratorCache&&) = delete;

  protected:
    FeatureGeneratorCache(){}
    ~FeatureGeneratorCache(){}
};
