#pragma once

#include <vector>
#include <unordered_map>
#include <map>
#include <string>
#include <functional>
#include <cmath>

class FeaturePreparation {
  public:
    /** Setup the functions to prepare given feature vectors
     */
    void setFunctions(const std::vector<std::string>& transformations);
    void setFunctions(const std::vector<std::string>& featureNames,
                      const std::map<std::string, std::string>& functionMap);

    /** Calls all previously defined functions on corresponding features
     */
    std::vector<double> prepareFeatures(std::vector<double> features);

  private:
    static const std::unordered_map<std::string, std::function<double(double)>> m_functions;
    std::vector<std::function<double(double)>> m_activeFunctions = {};
};
