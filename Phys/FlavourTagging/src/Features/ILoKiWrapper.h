#pragma once

#include "Event/Particle.h"

static const InterfaceID IID_ILoKiWrapper("ILoKiWrapper", 1, 0);

class ILoKiWrapper: virtual public IAlgTool {
  public:
    static const InterfaceID& interfaceID() { return IID_ILoKiWrapper; }
    virtual std::function<double()> getFunctor(const std::string& functorName) = 0;
    virtual void setOutlets(const LHCb::Particle* const* pTaggingParticle,
                            const LHCb::Particle* const* pSignalCandidate,
                            const LHCb::RecVertex* const* pVertexBase) = 0;
};
