#pragma once

#include "Event/MCParticle.h"

#include <vector>

static const InterfaceID IID_IMonteCarloFeatures("IMonteCarloFeatures", 1, 0);

class IMonteCarloFeatures: virtual public IAlgTool {
  public:
    static const InterfaceID& interfaceID() { return IID_IMonteCarloFeatures; }
    virtual std::vector<const LHCb::MCParticle*> getMCParticles() = 0;
    virtual const LHCb::MCParticle* signalParticle() = 0;
    virtual void readParticles() = 0;
};
