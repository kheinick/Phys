#pragma once

#include "GaudiAlg/GaudiTupleTool.h"

static const InterfaceID IID_ITupleWriter("ITupleWriter", 1, 0);

class ITupleWriter : virtual public IAlgTool {
  public:
    static const InterfaceID& interfaceID() {return IID_ITupleWriter;}
    virtual void writeColumn(const std::vector<std::string> &variableNames,
                             const std::vector<double> &values) = 0;
    virtual void writeArray(const std::vector<std::string> &variableNames) = 0;
    virtual void setPrefix(const std::string &prefix) = 0;
    virtual void setTupleName(const std::string &name) = 0;
    virtual void addArrayRow(const std::vector<double> &values) = 0;
};
