#include "LoKiWrapper.h"
#include "LoKi/ProtoParticles.h"
#include "LoKi/BasicFunctors.h"

DECLARE_COMPONENT(LoKiWrapper)

LoKiWrapper::LoKiWrapper(const std::string& type,
                         const std::string& name,
                         const IInterface* interface) :
  GaudiTool(type, name, interface)
{
  declareInterface<ILoKiWrapper>(this);
}

StatusCode LoKiWrapper::initialize(){
  auto sc = GaudiTool::initialize();
  if(sc.isFailure()) return sc;

  m_factory = tool<LoKi::IHybridFactory>(
      "LoKi::Hybrid::Tool/HybridFactory:PUBLIC"
  );

  return StatusCode::SUCCESS;
}

std::function<double()> LoKiWrapper::getFunctor(const std::string& functorName)
{
  // set output level to FATAL, since loki will return ERROR if a functor could
  // not be parsed in the first try
  Gaudi::Utils::setProperty(m_factory, "OutputLevel", MSG::FATAL);

  // currently there are 2 signatures of LoKi functos:
  // - Fun: double(const LHCb::Particle*)
  // - VFun: double(const LHCb::VertexBase*)
  // - and more... This might be a bad solution
  // @TODO: Find a beter one
  // First try to initialize Fun, if that fails, initialize VFun
  auto fName_ = functorName;
  auto particleHandle = m_pTaggingParticle;
  if(boost::starts_with(fName_, "Signal_")){
    particleHandle = m_pSignalCandidate;
    fName_.erase(0, std::string("Signal_").size());
  }
  StatusCode sc;
  LoKi::PhysTypes::Fun particleFunctor = LoKi::BasicFunctors<const LHCb::Particle*>::Constant(0);
  sc = m_factory->get(fName_, particleFunctor);
  if(sc == StatusCode::SUCCESS){
    if(msgLevel(MSG::DEBUG)) debug() << "[LoKiWrapper::getFunctor] "
      << "Initializing feature " << functorName << " as particleFunctor."
      << endmsg;
    return [particleFunctor, particleHandle]() -> double {
      return particleFunctor(*particleHandle);
    };
  }

  LoKi::PhysTypes::VFun vertexFunctor = LoKi::BasicFunctors<const LHCb::VertexBase*>::Constant(0);
  sc = m_factory->get(functorName, vertexFunctor);
  if(sc == StatusCode::SUCCESS){
    if(msgLevel(MSG::DEBUG)) debug() << "[LoKiWrapper::getFunctor] "
      << "Initializing feature " << functorName << " as vertexFunctor."
      << endmsg;
    return [vertexFunctor, this]() -> double {
      return vertexFunctor(*this->m_pAssociatedVertex);
    };
  }

  // only for testing
  // @TODO: remove this and either find a way to implement these lokifunctors
  // in a more general way or implement those inside feature_generator as
  // custom functors.
  LoKi::BasicFunctors<const LHCb::ProtoParticle*>::PredicateFromPredicate
    protoParticleFunctorB = LoKi::Constant<const LHCb::ProtoParticle *, bool>(false);
  sc = m_factory->get(functorName, protoParticleFunctorB);
  if(sc == StatusCode::SUCCESS){
    if(msgLevel(MSG::DEBUG)) debug() << "[LoKiWrapper::getFunctor] "
      << "Initializing feature " << functorName << " as protoParticleFunctor."
      << endmsg;
    return [protoParticleFunctorB, this]() -> double {
      return protoParticleFunctorB((*this->m_pTaggingParticle)->proto());
    };
  }
  LoKi::BasicFunctors<const LHCb::ProtoParticle*>::FunctionFromFunction
    protoParticleFunctor = LoKi::Constant<const LHCb::ProtoParticle *, double>(0);
  sc = m_factory->get(functorName, protoParticleFunctor);
  if(sc == StatusCode::SUCCESS){
    if(msgLevel(MSG::DEBUG)) debug() << "[LoKiWrapper::getFunctor] "
      << "Initializing feature " << functorName << " as protoParticleFunctor."
      << endmsg;
    return [protoParticleFunctor, this]() -> double {
      return protoParticleFunctor((*this->m_pTaggingParticle)->proto());
    };
  }

  if(msgLevel(MSG::FATAL)) fatal() << "[LoKiWrapper::getFunctor] "
    << "Feature " << functorName << " could not be initialized." << endmsg;
  throw std::invalid_argument("Requested feature not found. To define a "
        "manual feature, use prefix `man_`. Otherwise make sure to implement a"
        " custom feature or use a correct LoKi functor.");
  return []() -> double {
    return 0;
  };
}

void LoKiWrapper::setOutlets(const LHCb::Particle* const* pTaggingParticle,
                             const LHCb::Particle* const* pSignalCandidate,
                             const LHCb::RecVertex* const* pVertexBase)
{
  m_pTaggingParticle = pTaggingParticle;
  m_pSignalCandidate = pSignalCandidate;
  m_pAssociatedVertex = pVertexBase;
}
