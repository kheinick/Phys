#pragma once

#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Property.h"
#include "GaudiKernel/StatusCode.h"
#include "Event/Particle.h"
#include "LoKi/IHybridFactory.h"
#include "LoKi/PhysTypes.h"

#include <string>
#include <vector>
#include <map>
#include <functional>

#include "boost/algorithm/string/predicate.hpp"

#include "ILoKiWrapper.h"

class LoKiWrapper : public GaudiTool,
                    virtual public ILoKiWrapper {
  public:
    LoKiWrapper(const std::string& type,
                const std::string& name,
                const IInterface* interface);

    virtual StatusCode initialize() override;

    virtual std::function<double()> getFunctor(const std::string& functorName) override;

    virtual void setOutlets(const LHCb::Particle* const* pTaggingParticle,
                            const LHCb::Particle* const* pSignalCandidate,
                            const LHCb::RecVertex* const* pRecVertex) override;

  private:
    LoKi::IHybridFactory* m_factory = nullptr;
    const LHCb::Particle* const* m_pTaggingParticle = nullptr;
    const LHCb::Particle* const* m_pSignalCandidate = nullptr;
    const LHCb::RecVertex* const* m_pAssociatedVertex = nullptr;
};
