#pragma once

#include "GaudiAlg/GaudiTool.h"
#include "Event/MCParticle.h"
#include "MCInterfaces/IMCDecayFinder.h"

#include "IMonteCarloFeatures.h"

class MonteCarloFeatures : public GaudiTool,
                           virtual public IMonteCarloFeatures {
  public:
    MonteCarloFeatures(const std::string& type,
                       const std::string& name,
                       const IInterface* interface);

    virtual StatusCode initialize() override;

    virtual std::vector<const LHCb::MCParticle*> getMCParticles() override;

    virtual const LHCb::MCParticle* signalParticle() override;

    /* Read mc particles from tes
     * @TODO: maybe do this "globally" within the TaggingTool
     */
    virtual void readParticles() override;

  private:
    const LHCb::MCParticle* m_mcSignal = nullptr;
    std::vector<const LHCb::MCParticle*> m_mcParticles = {};

    IMCDecayFinder* m_mcDecayFinder = nullptr;
};
