#pragma once

#include <vector>
#include <map>
#include <iostream>
#include <stdexcept>
#include <utility>

#include "boost/regex.hpp"
#include "boost/log/core.hpp"
#include "boost/log/trivial.hpp"
#include "boost/log/expressions.hpp"

#include "src/Features/FeatureGenerator.h"
#include "src/Features/ITupleWriter.h"
#include "src/Utils/TaggingHelpers.h"

typedef std::function<bool(double)> Func;

class SelectionParser {
  public:
    void setFeatureAliases(const std::map<std::string, std::string>& aliases);
    void parseSelections(const std::vector<std::string>& selections);
    bool checkFeatures(const std::vector<double>& features);
    std::vector<std::string> getFeaturesNames() const;
    bool checkFeatureAtPosition(double& value, size_t& pos);

  private:
    static Func parseSelectionString(const std::string& selectionString);
    std::pair<std::string, std::string> parseFeatureSelectionString(const std::string& s);
    std::vector<Func> m_selectors = {};
    static const boost::regex m_regex_pattern;
    static const std::map<std::string, double> m_units;

    /* unique vector of all feature names that are used for selections
     */
    std::vector<std::string> m_featureNames;

    std::map<std::string, std::string> m_aliasMap,
                                       m_reverseAliasMap;

    /* vector of indices of selectors to join a vector of feature values with a
     * the vector of selectors
     * Store one index of a feature for each selector
     */
    std::vector<size_t> m_featureIndicesOfSelectors = {};

    /* vice versa, store all selector indices for each feature.
     */
    std::vector<std::vector<size_t>> m_selectorIndicesOfFeatures = {};
};
