#include "DevelopmentTagger.h"

DECLARE_COMPONENT(DevelopmentTagger)

using namespace std;
using namespace LHCb;

DevelopmentTagger::DevelopmentTagger(const string& type,
                                     const string& name,
                                     const IInterface* parent) :
  GaudiTool(type, name, parent)
{
  declareInterface<ITagger>(this);
}

StatusCode DevelopmentTagger::initialize()
{
  auto sc = GaudiTool::initialize();
  if(sc.isFailure()) return sc;

  m_pipeline = make_unique<Pipeline>();
  m_pipeline->setToolProvider(this);
  m_pipeline->setAliases(m_featureAliases);
  m_pipeline->setPipeline(m_selectionPipeline);

  return StatusCode::SUCCESS;
}

Tagger DevelopmentTagger::tag(const Particle* sigPart,
                              const RecVertex* assocVtx,
                              const int,
                              Particle::ConstVector& tagParts)
{
  Tagger tagObject;

  m_pipeline->setReconstructionVertex(assocVtx);
  m_pipeline->setSignalCandidate(sigPart);

  // apply selection to calculate features and fill cache
  m_pipeline->applySelection(tagParts);

  return tagObject;
}

Tagger DevelopmentTagger::tag(const Particle* sigPart,
                              const RecVertex* assocVtx,
                              RecVertex::ConstVector& puVtxs,
                              Particle::ConstVector& tagParts)
{
  m_pipeline->setPileUpVertices(puVtxs);
  return tag(sigPart, assocVtx, puVtxs.size(), tagParts);
}


vector<string> DevelopmentTagger::featureNames() const {
  return m_pipeline->mergedFeatureNames();
}

vector<double> DevelopmentTagger::featureValues() const {
  return m_pipeline->mergedFeatures();
}

vector<string> DevelopmentTagger::featureNamesTagParts() const {
  return m_pipeline->mergedFeatureNames();
}

vector<vector<double>> DevelopmentTagger::featureValuesTagParts() const {
  return m_pipeline->mergedFeatureMatrix();
}
