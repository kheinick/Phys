#pragma once

// from Gaudi
#include "GaudiAlg/GaudiTool.h"

// from LHCb
#include "Event/FlavourTag.h"

// from Phys
#include "Kernel/ITagger.h"

// local
#include "src/Selection/Pipeline.h"

/** @brief Pseudto-Tagger to produce optimization tuples.
 *
 * @authors    Kevin Heinicke, Julian Wishahi
 * @date       2017
 */
class DevelopmentTagger : public GaudiTool,
                          virtual public ITagger {

public:
  DevelopmentTagger(const std::string& type,
                    const std::string& name,
                    const IInterface* parent);

  StatusCode initialize() override;

  LHCb::Tagger::TaggerType taggerType() const override {
    return LHCb::Tagger::TaggerType::unknown;
  }

  LHCb::Tagger tag(const LHCb::Particle* sigPart,
                   const LHCb::RecVertex* assocVtx,
                   const int nPUVtxs,
                   LHCb::Particle::ConstVector&) override;

  LHCb::Tagger tag(const LHCb::Particle* sigPart,
                   const LHCb::RecVertex* assocVtx,
                   LHCb::RecVertex::ConstVector& puVtxs,
                   LHCb::Particle::ConstVector& tagParts) override;

  std::vector<std::string> featureNames() const override;

  std::vector<double> featureValues() const override;

  std::vector<std::string> featureNamesTagParts() const override;

  std::vector<std::vector<double>> featureValuesTagParts() const override;

private:
  Gaudi::Property<std::vector<std::vector<std::string>>> m_selectionPipeline{
    this, "SelectionPipeline", {{}}, ""
  };
  Gaudi::Property<std::map<std::string, std::string>> m_featureAliases{
    this, "Aliases", {}, ""
  };
  std::unique_ptr<Pipeline> m_pipeline = nullptr;
};
