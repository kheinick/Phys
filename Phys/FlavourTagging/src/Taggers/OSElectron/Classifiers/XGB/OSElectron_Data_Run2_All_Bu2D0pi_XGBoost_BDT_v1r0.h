#pragma once

#include <cmath>

#include "src/Classification/TaggingClassifierTMVA.h"

class OSElectron_Data_Run2_All_Bu2D0pi_XGBoost_BDT_v1r0 : public TaggingClassifierTMVA {
public:
  double GetMvaValue(const std::vector<double>& featureValues) const override;

private:
  double evaluateEnsemble(const std::vector<double>& featureValues) const;

  /* @brief sigmoid transformation */
  double sigmoid(double value) const; 
};

