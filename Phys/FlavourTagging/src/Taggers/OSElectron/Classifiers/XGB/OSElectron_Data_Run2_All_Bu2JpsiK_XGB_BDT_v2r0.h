#pragma once

#include <cmath>

#include "src/Classification/TaggingClassifierTMVA.h"

class OSElectron_Data_Run2_All_Bu2JpsiK_XGB_BDT_v2r0 : public TaggingClassifierTMVA {
public:
  double GetMvaValue(const std::vector<double>& featureValues) const override;

private:
  double evaluateEnsemble(const std::vector<double>& featureValues) const;

  /* @brief sigmoid transformation */
  double sigmoid(double value) const;
};
