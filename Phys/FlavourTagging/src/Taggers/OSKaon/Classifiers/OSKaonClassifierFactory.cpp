#include "OSKaonClassifierFactory.h"

// ITaggingClassifier implementations
#include "TMVA/OSKaon_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0.h"
#include "XGBoost/OSKaon_Data_Run2_All_Bu2JpsiK_XGBoost_BDT_v1r0.h"
#include "XGBoost/OSKaon_Data_Run2_All_Bu2D0pi_XGBoost_BDT_v1r0.h"
#include "XGBoost/OSKaon_Data_Run2_All_Bu2JpsiK_XGBoost_BDT_v2r0.h"

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( OSKaonClassifierFactory )

OSKaonClassifierFactory::OSKaonClassifierFactory(const std::string& type,
                                                 const std::string& name,
                                                 const IInterface* parent)
  : GaudiTool(type, name, parent)
{
  declareInterface<ITaggingClassifierFactory>(this);
}

StatusCode OSKaonClassifierFactory::initialize() {
  auto sc = GaudiTool::initialize();
  if(sc.isFailure()) return sc;

  addTMVAClassifier<OSKaon_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0>("OSKaon_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0");
  addTMVAClassifier<OSKaon_Data_Run2_All_Bu2JpsiK_XGBoost_BDT_v1r0>("OSKaon_Data_Run2_All_Bu2JpsiK_XGBoost_BDT_v1r0");
  addTMVAClassifier<OSKaon_Data_Run2_All_Bu2D0pi_XGBoost_BDT_v1r0>("OSKaon_Data_Run2_All_Bu2D0pi_XGBoost_BDT_v1r0");
  addTMVAClassifier<OSKaon_Data_Run2_All_Bu2JpsiK_XGBoost_BDT_v2r0>("OSKaon_Data_Run2_All_Bu2JpsiK_XGBoost_BDT_v2r0");

  return sc;
}


std::unique_ptr<ITaggingClassifier> OSKaonClassifierFactory::taggingClassifier()
{
  return m_classifierMapTMVA[m_classifierName]();
}

std::unique_ptr<ITaggingClassifier> OSKaonClassifierFactory::taggingClassifier(const std::string& type,
                                                                               const std::string& name)
{
  if (type == "TMVA") {
    return m_classifierMapTMVA[name]();
  } else {
    error() << "Classifier of type " << type << " unknown!" << endmsg;
    return nullptr;
  }
}
