#ifndef OSMUON_CLASSIFIER_DATA_RUN1_ALL_BU2JPSIK_TMVA_MLP_v1r0_H
#define OSMUON_CLASSIFIER_DATA_RUN1_ALL_BU2JPSIK_TMVA_MLP_v1r0_H 1

#include "src/Classification/TaggingClassifierTMVA.h"

class OSMuon_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0 : public TaggingClassifierTMVA {

public:
  OSMuon_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0();

  double GetMvaValue(const std::vector<double>& featureValues) const override;
  
private:
  // method-specific destructor
  void Clear();

  // input variable transformation

  double fDecTF_1[3][9][9];

  double fMin_2[3][9];
  double fMax_2[3][9];
  void InitTransform_1();
  void Transform_1( std::vector<double> & iv, int sigOrBgd ) const;
  void InitTransform_2();
  void Transform_2( std::vector<double> & iv, int sigOrBgd ) const;
  void InitTransform();
  void Transform( std::vector<double> & iv, int sigOrBgd ) const;

  // common member variables
  const char* fClassName = "OSMuon_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0";

  const size_t fNvars = 9;
  size_t GetNvar()           const { return fNvars; }
  char   GetType( int ivar ) const { return fType[ivar]; }

  // normalisation of input variables
  const bool fIsNormalised = false;
  bool IsNormalised() const { return fIsNormalised; }
  double fVmin[9] = {-1, -1, -1, -1, -1, -1, -1, -1, -1};
  double fVmax[9] = {0.99999988079071, 1, 1, 1, 1, 1, 1, 1, 1};

  double NormVariable( double x, double xmin, double xmax ) const {
     // normalise to output range: [-1, 1]
     return 2*(x - xmin)/(xmax - xmin) - 1.0;
  }

  // type of input variable: 'F' or 'I'
  char   fType[9] = {'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F'};

  // initialize internal variables
  void Initialize();
  double GetMvaValue__( const std::vector<double>& inputValues ) const;

  // private members (method specific)

  double ActivationFnc(double x) const;
  double OutputActivationFnc(double x) const;

  int fLayers;
  int fLayerSize[3];
  double fWeightMatrix0to1[15][10];   // weight matrix from layer 0 to 1
  double fWeightMatrix1to2[1][15];   // weight matrix from layer 1 to 2

  double * fWeights[3];
};

#endif // OSMUON_CLASSIFIER_DATA_RUN1_ALL_BU2JPSIK_TMVA_MLP_v1r0_H

