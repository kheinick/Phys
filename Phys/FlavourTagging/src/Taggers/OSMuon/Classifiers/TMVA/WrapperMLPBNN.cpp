#include "WrapperMLPBNN.h"


// hack, otherwise: redefinitions...
#ifndef SKIP_TMVA
  #include "weights/muon__muonMLPBNN.class.C"
#endif


namespace Taggers {
namespace OSMuon {
namespace Classifiers {
namespace TMVA {

WrapperMLPBNN::WrapperMLPBNN(std::vector<std::string> & names) {
#ifdef SKIP_TMVA
  int size = names.size();
  if (size == 0)
    std::cout << "WARNING: NO VALUES PASSED" << std::endl;
#else
  reader = new Read_muonMLPBNN(names);
#endif
}

WrapperMLPBNN::~WrapperMLPBNN() {
#ifndef SKIP_TMVA
  delete reader;
#endif
}


double WrapperMLPBNN::GetMvaValue(std::vector<double> const & values) {
#ifdef SKIP_TMVA
  int size = values.size();
  if (size == 0)
    std::cout << "WARNING: NO VALUES PASSED" << std::endl;
  return 0.0;
#else
  return reader->GetMvaValue(values);
#endif
  return 0.0;
  
}

} // namespace TMVA
} // namespace Classifiers
} // namespace OSMuon
} // namespace Taggers
