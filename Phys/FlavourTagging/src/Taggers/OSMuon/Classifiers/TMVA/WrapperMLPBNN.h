#ifndef TAGGERS_OSMUON_CLASSIFIERS_TMVA_WRAPPERMLPBNN_H
#define TAGGERS_OSMUON_CLASSIFIERS_TMVA_WRAPPERMLPBNN_H 1

#include "src/TMVAWrapper.h"

namespace Taggers {
namespace OSMuon {
namespace Classifiers {
namespace TMVA {

// forward declaration
class Read_muonMLPBNN; 

class WrapperMLPBNN : public TMVAWrapper {
public:
  WrapperMLPBNN(std::vector<std::string> &);
  ~WrapperMLPBNN();
  double GetMvaValue(std::vector<double> const &) override;

private:
  Read_muonMLPBNN* reader;

};

} // namespace TMVA
} // namespace Classifiers
} // namespace OSMuon
} // namespace Taggers

#endif // TAGGERS_OSMUON_CLASSIFIERS_TMVA_WRAPPERMLPBNN_H
