#include "WrapperMLPBNNMC.h"



// hack, otherwise: redefinitions...
//
#ifndef SKIP_TMVA
#include "weights/muon__muonMLPBNN_MC.class.C"
#endif

namespace Taggers {
namespace OSMuon {
namespace Classifiers {
namespace TMVA {

WrapperMLPBNNMC::WrapperMLPBNNMC(std::vector<std::string> & names) {
#ifdef SKIP_TMVA
  int size = names.size();
  if (size == 0)
    std::cout << "WARNING: NO VALUES PASSED" << std::endl;
#else
	reader = new Taggers::OSMuon::Classifiers::TMVA::Read_muonMLPBNN_MC(names);
#endif
}

WrapperMLPBNNMC::~WrapperMLPBNNMC() 
{ 
#ifndef SKIP_TMVA
  delete reader;
#endif
}


double WrapperMLPBNNMC::GetMvaValue(std::vector<double> const & values) {
#ifdef SKIP_TMVA
  int size = values.size();
  if (size == 0)
    std::cout << "WARNING: NO VALUES PASSED" << std::endl;
  return 0.0;
#else
  return reader->GetMvaValue(values);
#endif
  return 0.0;
}

} // namespace TMVA
} // namespace Classifiers
} // namespace OSMuon
} // namespace Taggers
