#ifndef TAGGERS_OSMUON_CLASSIFIERS_TMVA_WRAPPERMLPBNNMC_H
#define TAGGERS_OSMUON_CLASSIFIERS_TMVA_WRAPPERMLPBNNMC_H 1

#include "src/TMVAWrapper.h"

namespace Taggers {
namespace OSMuon {
namespace Classifiers {
namespace TMVA {

// forward declaration 
class Read_muonMLPBNN_MC;  

class WrapperMLPBNNMC : public TMVAWrapper {
 public:
  WrapperMLPBNNMC(std::vector<std::string> &);
  ~WrapperMLPBNNMC();
  double GetMvaValue(std::vector<double> const &) override;

private:
  Read_muonMLPBNN_MC * reader;

};
} // namespace TMVA
} // namespace Classifiers
} // namespace OSMuon
} // namespace Taggers

#endif // TAGGERS_OSMUON_CLASSIFIERS_TMVA_WRAPPERMLPBNNMC_H
