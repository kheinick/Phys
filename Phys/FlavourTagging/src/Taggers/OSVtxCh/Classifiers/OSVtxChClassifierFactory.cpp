#include "OSVtxChClassifierFactory.h"

// ITaggingClassifier implementations
#include "TMVA/OSVtxCh_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0.h"

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( OSVtxChClassifierFactory )

OSVtxChClassifierFactory::OSVtxChClassifierFactory(const std::string& type,
                                                   const std::string& name,
                                                   const IInterface* parent)
  : GaudiTool(type, name, parent)
{
  declareInterface<ITaggingClassifierFactory>(this);
}

StatusCode OSVtxChClassifierFactory::initialize() {
  auto sc = GaudiTool::initialize();
  if(sc.isFailure()) return sc;

  addTMVAClassifier<OSVtxCh_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0>("OSVtxCh_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0");

  return StatusCode::SUCCESS;
}


std::unique_ptr<ITaggingClassifier> OSVtxChClassifierFactory::taggingClassifier()
{
  return m_classifierMapTMVA[m_classifierName]();
}

std::unique_ptr<ITaggingClassifier> OSVtxChClassifierFactory::taggingClassifier(const std::string& type,
                                                                               const std::string& name)
{
  if (type == "TMVA") {
    return m_classifierMapTMVA[name]();
  } else {
    error() << "Classifier of type " << type << " unknown!" << endmsg;
    return nullptr;
  }
}
