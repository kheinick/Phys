#ifndef VERTEXOSWRAPPER_H
#define VERTEXOSWRAPPER_H 1

#include "src/TMVAWrapper.h"

namespace MyVertexOSSpace { class Read_vtxMLPBNN; }

class VertexOSWrapper : public TMVAWrapper {
public:
	VertexOSWrapper(std::vector<std::string> &);
	~VertexOSWrapper();
	double GetMvaValue(std::vector<double> const &) override;

private:
	MyVertexOSSpace::Read_vtxMLPBNN * reader;

};

#endif
