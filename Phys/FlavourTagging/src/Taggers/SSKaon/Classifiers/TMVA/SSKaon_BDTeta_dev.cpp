#include "SSKaon_BDTeta_dev.h"

// hack, otherwise: redefinitions...
//
namespace MyBDTetaSpace 
{
#include "weights/BDTeta_SSK_dev/TMVAClassification_BDTG_BsBsb_6vars_BDTGselTCut_0.72.class.C"
}

BDTetaReaderCompileWrapper::BDTetaReaderCompileWrapper(std::vector<std::string> & names) :
	BDTetareader(new MyBDTetaSpace::ReadBDTG_BsBsb_6vars_BDTGselTCut_072(names))
{}

BDTetaReaderCompileWrapper::~BDTetaReaderCompileWrapper() {delete BDTetareader; }

double BDTetaReaderCompileWrapper::GetMvaValue(std::vector<double> const & values) 
{
	return BDTetareader->GetMvaValue(values);
}
