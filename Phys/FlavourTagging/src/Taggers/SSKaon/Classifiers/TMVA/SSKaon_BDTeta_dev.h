#include <vector>
#include <string>
#include <cmath>
#include <iostream>

namespace MyBDTetaSpace { class ReadBDTG_BsBsb_6vars_BDTGselTCut_072; }

struct BDTetaReaderCompileWrapper{

	BDTetaReaderCompileWrapper(std::vector<std::string> &);

	~BDTetaReaderCompileWrapper();

	double GetMvaValue(std::vector<double> const &);

	private:
	MyBDTetaSpace::ReadBDTG_BsBsb_6vars_BDTGselTCut_072 * BDTetareader;

};
