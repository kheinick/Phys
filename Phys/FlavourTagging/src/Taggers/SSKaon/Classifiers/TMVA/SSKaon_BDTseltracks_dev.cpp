#include "SSKaon_BDTseltracks_dev.h"

// hack, otherwise: redefinitions...
//
namespace MyBDTseltracksSpace 
{
#include "weights/BDTseltracks_SSK_dev/TMVAClassification_BDTG_selBestTracks_14vars.class.C"

}


BDTseltracksReaderCompileWrapper::BDTseltracksReaderCompileWrapper(std::vector<std::string> & names) :
	BDTseltracksreader(new MyBDTseltracksSpace::ReadBDTG_selBestTracks_14vars(names))
{}

BDTseltracksReaderCompileWrapper::~BDTseltracksReaderCompileWrapper() {delete BDTseltracksreader; }

double BDTseltracksReaderCompileWrapper::GetMvaValue(std::vector<double> const & values) 
{
	return BDTseltracksreader->GetMvaValue(values);
}

