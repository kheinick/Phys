#include <vector>
#include <string>
#include <cmath>
#include <iostream>

namespace MyBDTseltracksSpace { class ReadBDTG_selBestTracks_14vars; }

struct BDTseltracksReaderCompileWrapper{

	BDTseltracksReaderCompileWrapper(std::vector<std::string> &);

	~BDTseltracksReaderCompileWrapper();

	double GetMvaValue(std::vector<double> const &);

	private:
	MyBDTseltracksSpace::ReadBDTG_selBestTracks_14vars * BDTseltracksreader;

};
