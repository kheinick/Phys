#include "SSPionClassifierFactory.h"

// ITaggingClassifier implementations
#include "TMVA/SSPion_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r0.h"

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SSPionClassifierFactory )

SSPionClassifierFactory::SSPionClassifierFactory(const std::string& type,
                                                 const std::string& name,
                                                 const IInterface* parent)
  : GaudiTool(type, name, parent)
{
  declareInterface<ITaggingClassifierFactory>(this);
}

StatusCode SSPionClassifierFactory::initialize() {
  auto sc = GaudiTool::initialize();
  if(sc.isFailure()) return sc;

  addTMVAClassifier<SSPion_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r0>("SSPion_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r0");

  return StatusCode::SUCCESS;
}


std::unique_ptr<ITaggingClassifier> SSPionClassifierFactory::taggingClassifier()
{
  return taggingClassifier(m_classifierType, m_classifierName);
}

std::unique_ptr<ITaggingClassifier> SSPionClassifierFactory::taggingClassifier(const std::string& type,
                                                                               const std::string& name)
{
  if (type == "TMVA") {
    return m_classifierMapTMVA[name]();
  } else if (type == "XGBoost" && m_nameIsPath){
    auto classifier = std::make_unique<TaggingClassifierXGB>();
    classifier->setPath(m_classifierName);
    return std::move(classifier);
  } else {
    error() << "Classifier of type " << type << " unknown!" << endmsg;
    return nullptr;
  }
}
