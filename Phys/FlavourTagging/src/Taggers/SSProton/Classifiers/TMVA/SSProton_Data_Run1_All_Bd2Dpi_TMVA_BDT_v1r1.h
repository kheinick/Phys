#pragma once

#include <iostream>
#include <vector>
#include <cmath>

#include "src/Classification/TaggingClassifierTMVA.h"

/* @brief Split-up version of SSProton_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r0 BDT.
 * See https://its.cern.ch/jira/browse/LHCBPS-1726
 *
 * Generated using python bdt2cpp v0.1.2.
 */
class SSProton_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1 : public TaggingClassifierTMVA {

public:
  double GetMvaValue(const std::vector<double>& featureValues) const override
  {
    return normed(ensemble(featureValues));
  };

private:
  double tree_0(const std::vector<double>& features) const;
  double tree_1(const std::vector<double>& features) const;
  double tree_2(const std::vector<double>& features) const;
  double tree_3(const std::vector<double>& features) const;
  double tree_4(const std::vector<double>& features) const;
  double tree_5(const std::vector<double>& features) const;
  double tree_6(const std::vector<double>& features) const;
  double tree_7(const std::vector<double>& features) const;
  double tree_8(const std::vector<double>& features) const;
  double tree_9(const std::vector<double>& features) const;

  double ensemble(const std::vector<double>& features) const
  {
    return tree_0(features) + tree_1(features) + tree_2(features) +
           tree_3(features) + tree_4(features) + tree_5(features) +
           tree_6(features) + tree_7(features) + tree_8(features) +
           tree_9(features);
  };

  double normed(double value) const
  {
    return value / 0.341791;
  };

};
