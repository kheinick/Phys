#ifndef GENERICVERTEXFINDER_IGenericVertexFinder_H 
#define GENERICVERTEXFINDER_IGenericVertexFinder_H 1

// Include files
// -------------
#include "GaudiKernel/IAlgTool.h"
#include "Event/Particle.h"
#include "Event/Track.h"
#include "Event/Vertex.h"
#include "Event/RecVertex.h"
#include "Kernel/STLExtensions.h"

struct IGenericVertexFinder : extend_interfaces<IAlgTool>
{
 public:
  /// Declare interface ID
  DeclareInterfaceID( IGenericVertexFinder, 1, 0 );

  /// typedefs, subclasses, etc.
  struct ParticleWithVertex
  {
    std::unique_ptr<LHCb::Particle> particle ;
    std::unique_ptr<LHCb::Vertex>   vertex ;
  };
  using ParticleContainer = std::vector<ParticleWithVertex> ;
  using RecVertexContainer = std::vector<std::unique_ptr<LHCb::RecVertex> > ;
  
  /// returns a vector with particles and associated vertex
  virtual ParticleContainer findVertices( const LHCb::Particle::ConstVector& particles) const = 0;
  
  /// returns a vector of recvertices
  virtual RecVertexContainer findVertices( const LHCb::Track::Range& tracks ) const = 0;
};

#endif // TRACKINTERFACES_ITRACKCHI2CALCULATOR_H
