#ifndef TRACKINTERFACES_IPARTICLETRAJPROVIDER_H 
#define TRACKINTERFACES_IPARTICLETRAJPROVIDER_H 1

// Include files
// -------------
#include "GaudiKernel/IAlgTool.h"

// From TrackEvent
#include "TrajParticle.h"

static const InterfaceID IID_IParticleTrajProvider( "IParticleTrajProvider", 1, 0);

class IParticleTrajProvider : virtual public IAlgTool 
{
public:
  /// Retrieve interface ID
  static const InterfaceID& interfaceID() { 
    return IID_IParticleTrajProvider; }
  
  /// returns a TrajParticle object for use in vertexing
  virtual std::unique_ptr<LHCb::TrajParticle> trajectory( const LHCb::Particle& particle ) const = 0 ;
  virtual std::unique_ptr<LHCb::TrajParticle> trajectory( const LHCb::Track& track ) const = 0 ;
};

#endif
