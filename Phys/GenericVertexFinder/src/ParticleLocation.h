#ifndef PHYS_PARTICLELOCATION_H
#define PHYS_PARTICLELOCATION_H

//#include "Kernel/DaVinciStringUtils.h"
#include <boost/algorithm/string/erase.hpp>
#include <iostream>

namespace Kernel
{
  class ParticleLocation
  {
  private:
    mutable std::string m_value ;
    bool m_configured{false} ;
  public:
    ParticleLocation( std::string value = {} ) : m_value{ std::move(value) } {}
    bool empty() const { return m_value.empty() ; }
    std::string& value() { return m_value ; }
    const std::string& value() const { return m_value ; }
    std::string name() const { 
      if(!m_configured) configure() ;
      return m_value ; }
    std::string particles() const { return name() + "/Particles" ; }
    std::string vertices() const { return name() + "/decayVertices" ; }
  private:
    void configure() const {
      boost::algorithm::erase_last(m_value,"/Particles") ;
    }
  } ;

  // provide support for Gaudi::Property<ParticleLocation>
  inline std::string toString(const ParticleLocation& loc) { return loc.value() ; }
  inline std::ostream& toStream(const ParticleLocation& loc, std::ostream& os)
  { return os << std::quoted(toString(loc),'\''); }
  inline StatusCode parse(ParticleLocation& loc, const std::string& input ) {
    loc.value() = input; return StatusCode::SUCCESS ;}
  // and allow printout..
  inline std::ostream& operator<<(std::ostream& os, const ParticleLocation& s) { return toStream(s,os); }

}
#endif
