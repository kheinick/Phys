#ifndef PARTICLETRAJPROVIDER_H 
#define PARTICLETRAJPROVIDER_H 1

// Include files
// -------------
#include "GaudiKernel/IAlgTool.h"

// From TrackEvent
#include "TrajParticle.h"
class ITrackStateProvider ;

namespace ParticleTrajProvider
{
  // some global functions to create TrajParticles
  std::unique_ptr<LHCb::TrajParticle> trajectory(const ITrackStateProvider& stateprovider,
						 const LHCb::Particle& particle ) ;  
  std::unique_ptr<LHCb::TrajParticle> trajectory(const ITrackStateProvider& stateprovider,
						 const LHCb::Track& particle ) ;
}

#endif
