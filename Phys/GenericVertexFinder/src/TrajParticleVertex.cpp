#include "TrajParticleVertex.h"
namespace LHCb
{

  size_t TrajParticleVertex::numTracks( LHCb::Track::Types type ) const
  {
    return std::count_if( daughters.begin(), daughters.end(),
			  [type](const TrajParticle* dau) {
			    return dau->track() && dau->track()->type() == type;
			  } );
  }
  
  double TrajParticleVertex::mass() const
  {
    std::vector<double> masshypos( vertex->nTracks(),0) ;
    return vertex->mass( masshypos ) ;
  }
}
