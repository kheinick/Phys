#ifndef TRAJPARTICLEVERTEX_H
#define TRAJPARTICLEVERTEX_H

#include <vector>
#include "TrackKernel/TrackStateVertex.h"
#include "Event/Track.h"
#include "TrajParticle.h"

/** @class TrajParticleVertex TrajParticeVertex.h 
 *
 * Helper class for GenericVertexer: stores information on a vertex
 * candidate, including pointers to its TrajParticle daughters.
 *
 *  @author Wouter Hulsbergen
 **/

namespace LHCb
{  
  class TrackStateVertex ;
  
  struct TrajParticleVertex
  {
    typedef std::vector< const TrajParticle* > Daughters ;
    
    size_t numTracks(LHCb::Track::Types type) const ;
    double mass() const ;
    
    /// Default constructor
    TrajParticleVertex() = default ;
    
    /// add a non-trivial move constructor. pretty expensive, because of the vertex-traj relation.
    TrajParticleVertex( TrajParticleVertex&& source )
      : daughters{std::move(source.daughters)},
      vertex{std::move(source.vertex)}
    {
      for( const TrajParticle* dau : daughters )
	if( dau->mother()==&source ) dau->setMother(this) ;
    }
    /// add a non-trivial move assignment
    TrajParticleVertex& operator=( TrajParticleVertex&& source  )
    {
      daughters = std::move(source.daughters) ;
      vertex    = std::move(source.vertex) ;
      for( const TrajParticle* dau : daughters )
	if( dau->mother()==&source ) dau->setMother(this) ;
      return *this ;
    }
      
    /// remove the copy constructor and assignment operator
    TrajParticleVertex(  const TrajParticleVertex&  ) = delete ;
    TrajParticleVertex& operator=(const TrajParticleVertex&  ) = delete ;

    /// Add this vertex as mother to all its daughters
    void setAsMother()
    {
      for( const TrajParticle* dau : daughters )
	dau->setMother( this ) ;
    }

    /// Remove this vertex as mother to all its daughters. This used
    /// to be in the destructor but we seldom need it, and it is pretty
    /// expensive.
    void removeAsMother()
    {
      for( const TrajParticle* dau : daughters )
	if( dau->mother()==this )
	  dau->setMother(nullptr) ;
    }
    
    Daughters daughters ;
    std::unique_ptr<LHCb::TrackStateVertex> vertex ;
  } ;
}

#endif
