################################################################################
# Package: HighPtJets
################################################################################
gaudi_subdir(HighPtJets)

gaudi_depends_on_subdirs(Event/MCEvent
                         Phys/DaVinciKernel
                         Phys/DaVinciMCKernel
                         Tr/TrackInterfaces)

find_package(ROOT)
find_package(Boost)
include_directories(SYSTEM ${ROOT_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS})

gaudi_add_module(HighPtJets
                 src/*.cpp
                 INCLUDE_DIRS AIDA Tr/TrackInterfaces
                 LINK_LIBRARIES MCEvent DaVinciKernelLib DaVinciMCKernelLib)

