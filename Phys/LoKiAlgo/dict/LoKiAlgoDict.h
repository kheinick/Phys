// ============================================================================
#ifndef LOKI_LOKIALGODICT_H
#define LOKI_LOKIALGODICT_H 1
// ============================================================================
// redefined anyway in features.h by _GNU_SOURCE
#undef _XOPEN_SOURCE
#undef _POSIX_C_SOURCE
// Python must always be the first.
#include "Python.h"
// ============================================================================
// Include files
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/AlgoDecorator.h"
#include "LoKi/TupleDicts.h"
#include "LoKi/LoKiAlgo.h"
#include "LoKi/ExtCalls.h"
#include "LoKi/LoopChild.h"
#include "LoKi/LoopDecorator.h"
// ============================================================================
namespace LoKiAlgoDict
{
  // ===========================================================================
  struct __Instantiations
  {
    LoKi::Dicts::ExtFunCalls<LHCb::Particle>   m_c1 ;
    LoKi::Dicts::ExtFunCalls<LHCb::VertexBase> m_c2 ;
    LoKi::Dicts::ExtCutCalls<LHCb::Particle>   m_c3 ;
    LoKi::Dicts::ExtCutCalls<LHCb::VertexBase> m_c4 ;
    //
    LoKi::Dicts::Alg<LoKi::Algo>               m_a1 ;
    // fictive constructor
    __Instantiations();
    ~__Instantiations();
  } ;
  // ==========================================================================
} // end of anonymous namespace
// ============================================================================
// The END
// ============================================================================
#endif // LOKI_LOKIALGODICT_H
// ============================================================================
