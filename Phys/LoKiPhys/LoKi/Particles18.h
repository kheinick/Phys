// ============================================================================
#ifndef LOKI_PARTICLES18_H
#define LOKI_PARTICLES18_H 1
// ============================================================================
// Include files
// ============================================================================
// LoKiCore
// ============================================================================
#include "LoKi/ExtraInfo.h"
// ============================================================================
// LoKiPhys
// ============================================================================
#include "LoKi/PhysTypes.h"
// ============================================================================
namespace LoKi
{
  // ==========================================================================
  namespace Particles
  {
    // ========================================================================
    /** @class HasInfo
     *  Trivial predicate which evaluates LHCb::Particle::hasInfo
     *  function
     *
     *  It relies on the method LHCb::Particle::hasInfo
     *
     *  @see LHCb::Particle
     *  @see LoKi::Cuts::HASINFO
     *
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-15
     */
    class GAUDI_API HasInfo
      : public LoKi::ExtraInfo::CheckInfo<const LHCb::Particle*>
    {
    public:
      // ======================================================================
      // constructor from the key
      HasInfo( int key )
        : LoKi::AuxFunBase ( std::tie ( key ) )
        , LoKi::ExtraInfo::CheckInfo<const LHCb::Particle*> ( key )
      {}
      /// clone method (mandatory!)
      HasInfo* clone() const override { return new HasInfo(*this) ; }
      /// the specific printout
      std::ostream& fillStream( std::ostream& s ) const override ;
      // ======================================================================
    };
    // ========================================================================
    /** @class Info
     *  Trivial function which evaluates LHCb::Particle::info
     *
     *  It relies on the method LHCb::Particle::info
     *
     *  @see LHCb::Particle
     *  @see LoKi::Cuts::INFO
     *
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-15
     */
    class GAUDI_API Info
      : public LoKi::ExtraInfo::GetInfo<const LHCb::Particle*, double>
    {
    public:
      // ======================================================================
      // constructor from the key
      Info( int    key , double def )
        : LoKi::AuxFunBase ( std::tie ( key , def ) )
        , LoKi::ExtraInfo::GetInfo<const LHCb::Particle*> ( key , def )
      {}
      /// clone method (mandatory!)
      Info* clone() const override { return new Info(*this); }
      /// the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
  } //                                         end of namepsace LoKi::Particles
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_PARTICLES18_H
// ============================================================================
