################################################################################
# Package: ParticleMaker
################################################################################
gaudi_subdir(ParticleMaker)

gaudi_depends_on_subdirs(Calo/CaloInterfaces
                         Calo/CaloUtils
                         Event/RecEvent
                         Phys/DaVinciKernel
                         Phys/DaVinciTools
                         Tr/TrackInterfaces)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(ParticleMaker
                 src/*.cpp
                 INCLUDE_DIRS Tr/TrackInterfaces
                 LINK_LIBRARIES CaloUtils RecEvent DaVinciKernelLib)

