// $Id:  $
// Include files

#include "Event/HltDecReports.h"
#include "Event/HltSelReports.h"

// local
#include "Hlt1TriggerTisTos.h"

//-----------------------------------------------------------------------------
// Implementation file for class : Hlt1TriggerTisTos
//
// 2010-06-23 : Tomasz Skwarnicki
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( Hlt1TriggerTisTos )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
Hlt1TriggerTisTos::Hlt1TriggerTisTos( const std::string& type,
                                  const std::string& name,
                                  const IInterface* parent )
  : TriggerTisTos ( type, name , parent )
{
  declareInterface<ITriggerTisTos>(this);
  setProperty("HltDecReportsLocation", "Hlt1/DecReports" );
  setProperty("HltSelReportsLocation", "Hlt1/SelReports" );
}

//=============================================================================
// Destructor
//=============================================================================
Hlt1TriggerTisTos::~Hlt1TriggerTisTos() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode Hlt1TriggerTisTos::initialize() 
{
  const StatusCode sc = TriggerTisTos::initialize(); 
  if ( sc.isFailure() ) return sc; 

  return sc;
}
